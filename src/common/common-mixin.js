export default {
  data() {
    return {
      devicePlatform: 'desktop',
      confirmationRequest: null,
      confirmationText: '',
    }
  },
  mounted() {
    const handleResize = () => {
      const initial = [
        {
          size: 1200,
          key: 'desktop'
        },
        {
          size: 800,
          key: 'tablet'
        },
        {
          size: 0,
          key: 'mobile'
        }
      ];
      let width = initial[0];
      for (let i = 0; i < initial.length; ++i) {
        if (window.innerWidth > initial[i].size) {
          width = initial[i];
          break;
        }
      }
      this.devicePlatform = width.key;
    };
    window.addEventListener('resize', handleResize);
    handleResize();
  },
  methods: {
    cancelConfirmAction() {
      this.confirmationRequest = null;
      this.confirmationText = '';
    }
  }
}